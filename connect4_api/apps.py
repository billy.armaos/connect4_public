from django.apps import AppConfig


class Connect4ApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'connect4_api'
