# Connect-4 API

## Introduction
This is an API implementation of the Connect-4 game (https://en.wikipedia.org/wiki/Connect_Four).
The API for this project is built using the Django REST framework (DRF).
Gunicorn is used as a webserver and Nginx as a reverse proxy.
The whole project is containerized with docker and SQLite is used as a database.
On top of these tools, djangorestframework-simplejwt is used to allow JSON Web Token (JWT) authentication.
Also, openapi/swagger-ui is used to provide the API documentation.
Finally, APScheduler is used to schedule the database maintenance.

## How To Run
The source code for this project can be found in this repository. 
Feel free to browse around to figure out the specifics of this particular implementation. 

To run the API using docker just run:
```bash
docker run --name connect4 -p 80:80 -e SECRET_KEY=YOUR_SECRET_KEY -d billarmaos/connect4
```
This command will run the API on port 80 of your system.
Please note that it is important to specify a secret key. 
The API will not run unless you specify one.
If you need a way to quickly generate a secret key, you can use an online generator like https://djecrety.ir/ (not recommended for production).

## Using the API
There are two main ways to use the API.
Both are described in the sub-sections "Intended Use" and "Browsable API" bellow.
The "Intended Use" section describes how one should use the API programmatically. 
The "Browsable API" section on the other hand has been included to allow ease of access, through a more friendly interface.

If at any point you need more details about the API schema, you can visit `/swagger-ui/`. 
Keep in mind however, that swagger-ui only displays the methods that the user has access to.
If you want to see the schema of a method that requires authentication you need to log in first.

### Browsable API
The browsable API comes out of the box with the use of DRF.
To enable its use, session authentication needs to be enabled.
This is not intended to be used in production since it is not as safe as the JWT authentication.
It has only been added for the ease of use that it provides to a human user of the API.

To use the browsable API, simply navigate to localhost with your browser. 
This takes you to the API root. 
From there you can click on the links for users or games to navigate to the corresponding endpoints.

Assuming this is your first visit at the API, you will see that there are no users nor games currently.
The first thing you need to do is to create two users. 
This can be done by navigating to `/users/` and then sending two POST requests using the HTML form that is provided at the bottom of the page.

Once you have created the users, you can log in using the credentials of either of the accounts you created.
To do this, just navigate to `/api-auth/login/` or use the handy "Log in" button at the top right of the page.

After logging in, you can navigate to `/games/` and send a POST request specifying the user that you want to start a game with.
This creates a game that you can navigate to using its id (just navigate to `/games/GAME_ID/`).
In this implementation of connect4, the owner (creator of the game instance) always makes the first move.
So go ahead and issue a PUT request with the Connect4 column number that you want to place your token at.
You now need to wait for the next player to make a move.

### Intended Use
The browsable API makes it easier for the user to navigate and make requests. 
However, this is not the way the API is supposed to be used. 
Specifically, the session authentication required for the browsable API to work is not required nor suggested for production.
Instead, JWT authentication is preferred.
In the following we describe a potential use scenario for the API using only JWT authentication with cURL.

Here we assume once again that no users or games have been created.
Again, you will need at least two users to play the game.
To create a user use a command like the following:

```bash
curl -X POST -H 'Content-Type: application/json' -d '{"username": "USERNAME", "password": "PASSWORD", "password2": "PASSWORD"}' http://127.0.0.1/users/
```

Now that a user is register we need to get the authentication token for this user using a command like the following:

```bash
curl -X POST -H 'Content-Type: application/json' -d '{"username": "USERNAME", "password": "PASSWORD"}' http://127.0.0.1/api/token/
```
The response we get looks like the following:

```json
{
    "refresh": "REFRESH_TOKEN",
    "access": "ACCESS_TOKEN"
}
```
One can now make authenticated requests using the access_token by setting a header of the form:
```
"Authorization: Bearer ACCESS_TOKEN"
```
For example, to create a new game, the following command may be used:
```bash
curl -X POST -H 'Content-Type: application/json' -H "Authorization: Bearer ACCESS_TOKEN" -d '{"guest":"http://127.0.0.1/users/USER_ID/"}' http://127.0.0.1/games/
```
And now, to place a token into a column you just need to use:
```bash
curl -X PUT -H 'Content-Type: application/json' -H "Authorization: Bearer ACCESS_TOKEN" -d '{"connect4_column":"COLUMN_ID"}' http://127.0.0.1/games/GAME_ID/
```
As a final note, keep in mind that the access token is short-lived. 
To obtain a new one once expired you can use the refresh token like so:  
```bash
curl -X POST -H 'Content-Type: application/json' -d '{"refresh":"REFRESH_TOKEN"}' http://127.0.0.1/api/token/refresh/
```

## Current Flaws
This piece of software is not ready for production. 
There are several things about it that still need to be changed or improved.
Here we outline the most important ones.

### SSL
This project does not use SSL.
This is by design since it was requested that the docker container runs on port 80.
This would have to change before the software reaches production.

### Multiple Processes in One Container
In the current state, three processes are running on a single docker container.
Specifically, nginx, gunicorn and the database maintenance process all run at the same container.
This is not good practice for production, but it was chosen because it makes shipping the code easier.
Docker-Compose should be used instead to isolate the processes.

### SQLite
This application uses SQLite as a database.
This is definitely not good practice for production. 
SQLite was chosen mainly because it allows the application to be shipped in a single container.
PostgreSQL is suggested for production instead.

### Session Authentication
As mentioned above, this project uses session authentication because it allows the use of the Browsable API.
This is not at all necessary for the API to work, therefore it is not considered a production feature.
On the contrary, it is more of a development feature, so that one can more easily inspect the structure of the API.
This needs to be removed for production (which is as simple as commenting out a single line).

### Database Representation
The game state is currently represented in the database as a 42-character string.
Although this currently does the job well, there is potential for improvement, possibly by storing the state in a JSON format.
