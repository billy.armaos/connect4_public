FROM nginx

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update
RUN apt-get install -y python3-pip

COPY ./requirements.txt .
RUN pip3 install -r requirements.txt

RUN mkdir  /source/

WORKDIR /source/

COPY . .

COPY connect4/static /static/static

COPY default.conf /etc/nginx/conf.d/default.conf

CMD ./startup.sh
