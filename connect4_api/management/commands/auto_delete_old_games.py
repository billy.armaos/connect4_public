from datetime import datetime, timedelta, timezone

from django.core.management.base import BaseCommand

from connect4_api.models import Game
from apscheduler.schedulers.background import BlockingScheduler


def delete_old_games():
    Game.objects.filter(is_finished=True).filter(
        updated_at__lt=datetime.now(timezone.utc) - timedelta(days=1)).delete()


class Command(BaseCommand):
    help = 'Deletes all games that have finished more than a day ago.'

    def handle(self, *args, **options):
        scheduler = BlockingScheduler(timezone='UTC')
        scheduler.add_job(delete_old_games, 'interval', hours=1)
        scheduler.start()
