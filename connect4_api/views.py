import itertools

import numpy as np
from django.contrib.auth.models import User
from rest_framework import viewsets, serializers

from .models import Game
from .permissions import UserSpecificPermission, GameSpecificPermission
from .serializers import UserSerializer, GameSerializer, UpdateGameSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [UserSpecificPermission]


class GameViewSet(viewsets.ModelViewSet):
    queryset = Game.objects.all().order_by('created_at')
    serializer_class = GameSerializer
    permission_classes = [GameSpecificPermission]

    def perform_create(self, serializer):
        if self.request.user == serializer.validated_data['guest']:
            raise serializers.ValidationError({'guest': 'You cannot start a game with yourself.'})
        serializer.save(owner=self.request.user, next_player=self.request.user)

    def get_serializer_class(self):
        if self.action == 'update':
            return UpdateGameSerializer
        return GameSerializer

    @staticmethod
    def find_winner(state_array):
        rows = state_array
        columns = state_array.T
        main_diagonals = (state_array.diagonal(i) for i in range(-3, 3))
        secondary_diagonals = (np.fliplr(state_array).diagonal(i) for i in range(-3, 3))

        for row in itertools.chain(rows, columns, main_diagonals, secondary_diagonals):
            if '1111' in ''.join(row):
                return '1'
            if '2222' in ''.join(row):
                return '2'
        return None

    def perform_update(self, serializer):
        instance = serializer.instance

        # Check if game is still ongoing.
        if instance.is_finished:
            raise serializers.ValidationError('This game is finished and does not longer accept new moves.')

        # Check if it's the turn of the player who makes the  request.
        if self.request.user != instance.next_player:
            raise serializers.ValidationError('Please wait your turn.')

        selected_column = serializer.validated_data['connect4_column']
        game_state = list(instance.state)

        # Get selected column and check if it is full.
        column_state = game_state[selected_column * 6:(selected_column + 1) * 6]
        try:
            first_empty_cel_iloc = next(i for i, cell in enumerate(column_state) if cell == '0')
        except StopIteration:
            raise serializers.ValidationError('Selected column is full. Please select another column.')

        # Assign the token to the appropriate cell.
        game_state[selected_column * 6 + first_empty_cel_iloc] = '1' if self.request.user == instance.owner else '2'

        # Get the winner if there is one.
        winner_id = self.find_winner(state_array=np.array(game_state).reshape(7, 6))

        # Dictionary that holds all the values of Game that we want to modify.
        to_be_modified_dict = {}

        if winner_id is not None:
            to_be_modified_dict['is_finished'] = True
            to_be_modified_dict['has_winner'] = True
            to_be_modified_dict['winner'] = instance.owner if winner_id == '1' else instance.guest

        # Check if the game still has empty cells for tokens.
        if '0' not in game_state:
            to_be_modified_dict['is_finished'] = True

        to_be_modified_dict['state'] = ''.join(game_state)
        to_be_modified_dict['next_player'] = instance.guest if self.request.user == instance.owner else instance.owner

        serializer.save(**to_be_modified_dict)
