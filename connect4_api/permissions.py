from rest_framework.permissions import BasePermission, SAFE_METHODS, IsAuthenticatedOrReadOnly


class UserSpecificPermission(BasePermission):

    def has_permission(self, request, view):
        if request.method in SAFE_METHODS + ('POST',):
            return True
        if request.method == 'DELETE' and request.user and request.user.is_authenticated:
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        if request.method == 'DELETE' and (request.user == obj or request.user.is_staff):
            return True
        return False


class GameSpecificPermission(IsAuthenticatedOrReadOnly):

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS + ('POST',):
            return True
        if request.method in ('PUT', 'DELETE') and (request.user in (obj.owner, obj.guest) or request.user.is_staff):
            return True
        return False
