from django.contrib.auth import password_validation
from django.contrib.auth.models import User
from django.core import exceptions
from rest_framework import serializers

from .models import Game


class UserSerializer(serializers.HyperlinkedModelSerializer):
    password = serializers.CharField(write_only=True)
    password2 = serializers.CharField(label='Repeat Password ', write_only=True)

    def validate(self, data):
        # get the password from the data
        password1 = data.get('password')
        password2 = data.pop('password2')

        if password1 != password2:
            raise serializers.ValidationError({'password2': "Passwords don\'t match."})

        user = User(data)

        errors = dict()
        try:
            # validate the password and catch the exception
            password_validation.validate_password(password=password1, user=user)

        # the exception raised here is different than serializers.ValidationError
        except exceptions.ValidationError as e:
            errors['password'] = list(e.messages)

        if errors:
            raise serializers.ValidationError(errors)

        return super(UserSerializer, self).validate(data)

    def create(self, validated_data):
        user = User.objects.create_user(
            username=validated_data['username'],
            password=validated_data['password'],
        )
        return user

    class Meta:
        model = User
        fields = ['url', 'username', 'password', 'password2', 'owned_games', 'guest_games', 'is_staff']
        read_only_fields = ['owned_games', 'guest_games', 'is_staff']


class GameSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Game
        fields = ['url', 'created_at', 'updated_at', 'owner', 'guest', 'is_finished', 'has_winner', 'winner',
                  'next_player', 'state']
        read_only_fields = ['owner', 'is_finished', 'has_winner', 'winner', 'next_player', 'state']


class UpdateGameSerializer(serializers.HyperlinkedModelSerializer):
    connect4_column = serializers.ChoiceField(choices=[0, 1, 2, 3, 4, 5, 6], write_only=True)

    class Meta:
        model = Game
        fields = ['connect4_column', 'url', 'created_at', 'updated_at', 'owner', 'guest', 'is_finished', 'has_winner',
                  'winner', 'next_player', 'state']
        read_only_fields = ['url', 'created_at', 'updated_at', 'owner', 'guest', 'is_finished', 'has_winner', 'winner',
                            'next_player', 'state']
