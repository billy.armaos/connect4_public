from django.db import models


class Game(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey('auth.User', related_name='owned_games', on_delete=models.CASCADE)
    guest = models.ForeignKey('auth.User', related_name='guest_games', on_delete=models.CASCADE,
                              help_text='Choose the user that you want to start the game with.')
    is_finished = models.BooleanField(default=False)
    has_winner = models.BooleanField(default=False)
    winner = models.ForeignKey('auth.User', related_name='games_won', on_delete=models.CASCADE, null=True)
    next_player = models.ForeignKey('auth.User', related_name='games_waiting', on_delete=models.CASCADE)
    state = models.CharField(max_length=42, default='0' * 42)

    def __str__(self):
        return f'Game created at {self.created_at} and updated at {self.updated_at}.'

    class Meta:
        ordering = ['created_at']
